﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

	public float maxS = 11f;


	private Rigidbody2D rb2d = null;
	private bool saltar = false;
	private float move = 0f;
	private bool attack = false;
	private bool muerto = false;
	private Animator anim;
	private bool flipped = false;
	private Vector3 origen;
	private float saltoy;
	public bool Ground = true;
	public Vector3 inipos;
	public AudioSource JumpAudio;
	public AudioSource AttackAudio;
	public AudioSource DeadAudio;




	// Use this for initialization
	void Awake()
	{
		origen = transform.position;


		rb2d = GetComponent<Rigidbody2D>();


		anim = GetComponent<Animator>();
		inipos.x = -8;
		inipos.y = -2;
		inipos.z = 0;
	}


	void FixedUpdate()
	{


		move = Input.GetAxis("Horizontal");
		if(Input.GetKeyDown(KeyCode.UpArrow) && Ground==true)
		{
			saltoy = 8;
			anim.SetBool ("Jump", true);
			JumpAudio.Play();
			Ground = false;
		}
		else
		{
			saltoy = 0;
			anim.SetBool ("Jump", false);
		}
		rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y+saltoy);
		if(Input.GetButton("Jump"))
		{
			attack = true;
			anim.SetBool("Attack", true);
			AttackAudio.Play ();
		}

		else
		{
			anim.SetBool("Attack", false);
		}

		saltar = Input.GetKeyDown(KeyCode.UpArrow);




		if(rb2d.velocity.x > 0.001f || rb2d.velocity.x < -0.001f)
		{
			if((rb2d.velocity.x < -0.001f && !flipped) || (rb2d.velocity.x > -0.001f && flipped))
			{
				flipped = !flipped;
				this.transform.rotation = Quaternion.Euler(0, flipped ? 180 : 0, 0);
			}
			anim.SetBool("Run", true);
		}
		else
		{
			anim.SetBool("Run", false);
		}
	}
	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "muerte") {
			muerto = true;
			anim.SetBool ("Dead", true);
			DeadAudio.Play ();
			Invoke ("reset", 0.7f);
		}
		if (coll.gameObject.tag == "Ground") {
			Ground = true;
			anim.SetBool ("Jump", false);
		}
		if (coll.gameObject.tag == "finish") {

			transform.position = new Vector3 (-8, -2, 0);
		}

	}
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "end") {
			SceneManager.LoadScene ("Win");
		}
	}
		



	void Restart()
	{
		this.transform.position = origen;
		anim.SetBool("Dead", false);
		muerto = false;
		anim.Play("Idle__001");
	}
	void OnBecameInvisible(){
		transform.position = new Vector3 (-8, -2, 0);
	}
	void reset(){
		transform.position = inipos;
	}

}